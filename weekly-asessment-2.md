1. Explain How the Internet Works.
   Internet is a long piece of wire and the wire connects defferent computers to each others. How's it work? let's say I am sitting at home on my laptop and I type in www.google.com beacuse I want to head over to the main google homepage. What happens behind the scenes is that my browser will send a message to my ISP (Internet Service Provider), and the ISP then relay that message to something called a DNS (Domain Name Server) server. Then, the DNS will look up in it's database as to what is the exact IP address of that website that I trying to access and every single computer/laptop that's connected to the internet has an IP adrress, this is like a postal for the computer/laptop. And one the DNS server find the IP address, it sends that back to my browser. So now I know the exact address where I can find the google homepage. The next thing that happens is I will send a direct request to that address, through the ISP, and this message will be delivered via internet wire to the server that is located address, such as 216.58.210.46. And on the server there is all of the files that I would need in order to be able to view the google homepage. Then, the server sends all those files back to me through the internet wire and I get to see the google homepage in my browser.

2. What's the meaning of communication protocol
   communication protocol is a set of rules and regulations that allow two electronic devices to connect to exchange the data with one and another

3. Mention HTTP status codes
   a. HTTP status code 1xx to response the information
   b. HTTP status code 2xx to response success
   c. HTTP status code 3xx to response redirection
   d. HTTP status code 4xx to response client error
   e. HTTP status code 5xx to response server error

4. Make Analogy Regarding how Frontend and Backend communicate each other
   It's kind of like restaurant. So there is a front of house, this is the main restaurant where the clients will sit down and have dinner. But also there's the kitchen where the cooking is mostly happening, and there is also the larder where we store all of the ingredients or everything we need to make the food. In this analogy of front of house restaurant is basically the client side (frontend), they are able to interact with the menu and see the information about the food. Then, we have got the kitchen and this is analogy to our server (backend), this is basically the place where all of the dishes get served from. And finally the larder where all the ingredients are stored is of course the database, this is where all of our data is stored. So, when the client ask for some food from the menu list, then that order sent to the kitchen, and the kitchen should sent back what it is that the client wanted, which is the food.

5. Mention What method in HTTP protocol
   GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS
